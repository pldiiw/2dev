(def project 'cuboid)
(def version "0.1.0-SNAPSHOT")

(set-env! :source-paths    #{"src"}
          :resource-paths  #{"resources"}
          :dependencies    '[[org.clojure/clojure "RELEASE"]
                             [quil "RELEASE"]
                             [boot-codox "RELEASE" :scope "test"]])

(task-options!
 aot  {:namespace   #{'cuboid.core}}
 pom  {:project     project
       :version     version
       :description "FIXME: write description"
       :url         "http://example/FIXME"
       :scm         {:url "https://gitlab.com/pldiiw/2dev.git"}
       :license     {"MIT License"
                     "https://opensource.org/licenses/MIT"}}
 jar  {:main        'cuboid.core
       :file        (str "cuboid-" version "-standalone.jar")}
 sift {:include     #{#"\.jar$"}})

(require '[codox.boot :refer [codox]])

(deftask build
  "Build the project locally as a JAR."
  [d dir PATH #{str} "the set of directories to write to (target)."]
  (let [dir (if (seq dir) dir #{"target"})]
    (comp (aot) (pom) (uber) (jar) (target :dir dir))))

(deftask run
  "Run the project."
  [a args ARG [str] "the arguments for the application."]
  (require '[cuboid.core])
  (apply (resolve 'cuboid.core/-main) args))
