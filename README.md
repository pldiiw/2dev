# cuboid

## Usage

Run the project directly:

    $ boot run

Build an uberjar from the project:

    $ boot build

Run the uberjar:

    $ java -jar target/cuboid-0.1.0-SNAPSHOT-standalone.jar [args]

## Documentation

Open the target/doc/index.html file in your favorite web browser.

If you can't access this file, regenerate the documentation with this command:

    $ boot codox -n cuboid target
