# Introduction

## Manual

### What is Cuboid?

Welcome to Cuboid!

Cuboid is remake of the eponymous game.

### Build

First of all, make sure you have Java 8 installed, at least.

Next, install `boot`, a clojure build tool: https://github.com/boot-clj/boot#install

Finally, run `boot build` to build the jar.

### Run

You can launch the jar or the directly the code without building it beforehand.

To launch the jar, run:

```
java -jar target/cuboid-*.jar
```

Preferably, launch the game with `boot run` instead, as the jar has some resource URL issues.

### How to play

We will see in this how to navigate through the various menus in the game and
how to play the actual game.

#### Main menu

Right after you've launched the game, you will put in front of the main menu.

You can see five buttons:
 * Play - This button will let play the game.
 * High Scores - This is where the best Cuboid players can be found.
 * Level Editor - If you have the soul of a level designer, it's the place to be.
 * Course Editor - It's the menu where you can manage and create your own level sequences.
 * Quit - Don't click on it now, your adventure has just started.

#### A little run

If you click on "Play", you will be presented a new menu.

 * Start A New Run - When you click on this button you can play with your courses or ours (which is called "Story Mode"").
 * Load A Previous Run - You can load an unfinished run.
 * Timer activated - Switch it on if you find the game too easy and seek more challenge.

Let's "Start a New Run" with "Story Mode" as the course.

You're in the game!

You're the little cuboid. You can move with the arrow keys or `zqsd`. Your goal
is reach the golden cube. Beware, each blocks on your way has a special
behiaviour.

Reaching the golden cube will lead to the next level, and so on until you finish
the course.

On a certain level, your cuboid will be split in two, letting you move each cube
independently. Use the space bar to switch between cubes.

#### High scores

This is where you can review the scores you've made in your previous runs.

#### Level editor

The level editor let you create your own levels.

You can place blocks with the left click. Change block type with the space bar
or by clicking the Selected button. Certain blocks can be linked to other
blocks, like teleports. Right click on one, you will enter in link editing mode.
Your left click now add and removes links attached your selected block. Right
click anywhere to return to the default mode. You can adjust the timer with the
`+` and `-` buttons. It is displayed in milliseconds. At any time, you can try
your level out with "Try out level" button. You can also undo and redo the edits
you've done with the "Undo" et "Redo" buttons. Save your level with the "Save
level" button.

#### Course editor

This editor let you create sequences of levels, courses.

You can add levels to the course by clicking the "+" button. If you need to
replace one, click on it too. Save it when ready.

## Documentation

### Architecture

The game is architectured in scenes and utility namespaces. Each scene is simply
a hash map of various functions that tells the scene orchetration code which
function should setup the scene, which function should update the scene, the
same for drawing, clicking, pressing a key, moving the mouse and destroying the
scene. The utility namespaces comes handy in manipulating all sorts of things.
From the level loading and drawing, to the coordination of movement, passing by
the game load/save.

Each of these generates the game state that gets passed around all functions.
Each uses just the state it needs, and update according to what's the user is
doing.

I would have loved to be able to deep dive into the depth of how this game
works, it's been a real thought challenge to come up with a proper way to handle
how a game evolves from one simple screen to many features and multiple screens.
But the clock it ticking, sadly.

### Graphic engine

We did not use any game engine, but used a graphic engine: Processing, with its
clojure bindings, Quil. This is normally a sketch library, but the game not
being a beast it suits the job.

