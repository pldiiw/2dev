(ns cuboid.util
  "Utilities."
  (:require [clojure.java.io :as io]))

(defn generic-names
  "Given a vector of paths, returns a vector of all *.edn files contained in
  each path."
  [paths]
  (->> paths
       (map (comp file-seq io/file))
       (flatten)
       (map #(.getName %1))
       (filter #(re-find #"^.*\.edn$" %1))
       (map #(subs %1 0 (- (count %1) 4)))
       (vec)))

(defn mkdirp
  "Make directories recursively."
  [path]
  (loop [current ""
         nexts (clojure.string/split path #"/")]
    (when (not-empty nexts)
      (let [next (str current (first nexts) "/")]
        (.mkdir (java.io.File. next))
        (recur next (rest nexts))))))
