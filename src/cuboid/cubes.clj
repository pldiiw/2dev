(ns cuboid.cubes
  "Functions to manipulate the cubes. In our game, there is no cuboid, only
  cubes. When they are next to each other, they become kind of tied to each
  other until they're unmerged. See the movement module for more information."
  (:require [quil.core :as q]))

(defn draw
  "Draw each cube."
  [[[x z :as pri] [x' z' :as sec]] cube-shape]
  (q/shape-mode :corner)
  (q/with-translation [x -1 z]
    (q/shape cube-shape))
  (q/with-translation [x' (if (= pri sec) -2 -1) z']
    (q/shape cube-shape)))

(defn draw-arrow
  "When splitted, a little arrow is drawn above the cube that is currently
  manipulated."
  [[[x z :as pri] sec] arrow-shape]
  (q/shape-mode :center)
  (q/with-translation [(+ x 0.5) -1.75 (+ z 0.5)]
    (q/push-matrix)
    ;; (q/rotate-x (* 0.5 q/HALF-PI))
    (q/shape arrow-shape)
    (q/pop-matrix)))
