(ns cuboid.run
  "When a user plays a course, a run is all the essential state needed to
  persist and restore his progress. To sum up, it's where the game saving and
  loading happens."
  (:require [cuboid.util :as util]
            [cuboid.level :as level]
            [clojure.edn :as edn]
            [cuboid.config :as config]
            [quil.core :as q]))

(defn names []
  (util/generic-names [config/user-runs-path]))

(defn save [name {started-at :level/started-at :as state}]
  (println started-at (:level/time state))
  (spit (str config/user-runs-path "/" name ".edn")
        (pr-str (-> state
                    (select-keys [:level/level
                                  :level/time
                                  :level/pos
                                  :course/name
                                  :course/course
                                  :course/looping
                                  :course/timer-enabled
                                  :score/score])
                    (update :level/time level/time-left started-at)))))

(defn load [name]
  (let [run (edn/read-string (slurp (str config/user-runs-path "/" name ".edn")))]
    (assoc run :level/started-at (q/millis))))
