(ns cuboid.course
  "A course is sequence of levels. This namespace contains functions to
  manipulate the courses' related state. Currently a course is described by 4
  keywords:
   * :course/course - A vector of level names, with the first element being the
  current/first level in the course, etc.
   * :course/name - The name of the course, as a str.
   * :course/timer-enabled - Whether the timer for the levels should be
  activated.
   * :course/looping - If true, when the player will reach the last level, the
  course will start all over, instead of redirecting to a screen to register its
  score."
  (:require [cuboid.level :as level]
            [clojure.edn :as edn]
            [cuboid.util :as util]
            [cuboid.config :as config]
            [cuboid.score :as score]
            [clojure.java.io :as io]))

(defn names []
  (util/generic-names [(io/resource config/builtin-courses-path) config/user-courses-path]))

(defn save [name {course :course/course}]
  (spit (str config/user-courses-path "/" name ".edn") (pr-str course)))

(defn load [name]
  (let [file   (or (io/resource (str config/builtin-courses-path "/" name ".edn"))
                   (str config/user-courses-path "/" name ".edn"))
        course (edn/read-string (slurp file))]
    {:course/course course}))

(defn setup [state]
  (let [name          (:course/name state config/default-course-name)
        course        (if-some [course (:course/course state)] {:course/course course} (load name))
        timer-enabled (:course/timer-enabled state false)
        looping       (:course/looping state false)]
    (merge state
           course
           {:course/name          name
            :course/timer-enabled timer-enabled
            :course/looping       looping})))

(defn destroy [state]
  (dissoc state
          :course/name
          :course/course
          :course/timer-enabled
          :course/looping))

(defn reset [state]
  (level/reset (score/reset (setup (destroy state)))))
