(ns demos.rotating-cube
  (:require [quil.core :as q :include-macros true]
            [quil.middleware :as m]))

(defn setup []
  (q/frame-rate 60)
  {:angle 0})

(defn update-state [state]
  (let [{:keys [angle]} state]
    {:angle (mod (+ angle 0.05) q/TWO-PI)}))

(defn draw-state [state]
  (q/background 255)
  (q/fill 227 181 5)
  (q/translate [(/ (q/width) 2) (/ (q/height) 2) 0])
  (q/rotate-x (* -0.55 q/HALF-PI))
  (q/rotate-y (:angle state))
  (q/box 250 250 250))

(q/defsketch my
  :host "host"
  :size [1000 1000]
  :setup setup
  :update update-state
  :draw draw-state
  :renderer :p3d
  :middleware [m/fun-mode])
