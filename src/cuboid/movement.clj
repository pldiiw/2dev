(ns cuboid.movement
  "This namespace takes care of handling every cube's movement. As said in the
  cubes namespace, technically there's no cuboid in this game, only cubes which
  are coordinated at every instant."
  (:require [quil.core :as q]))

(defn diff-vec
  "Returns the absolute difference of each element in the given vectors. This is
  used to check whether the cubes are next to each other or not."
  [pri sec]
  (map (comp #(Math/abs %1) -) pri sec))

(defn diff-card
  "Same as diff-vec, but returns the sum of all the elements of the two
  vectors."
  [pri sec]
  (reduce + (diff-vec pri sec)))

(defn re-merge
  "This is the coordinating part of the movement. Depending of where the cubes
  are positioned relative to each other, re-merge will correct their position to
  simulate how a cuboid would move."
  [[[x z :as pri] [x' z' :as sec]] cube-move]
  (cond
    (= pri sec)                  [(cube-move pri) (cube-move sec)] ; same place
    (= (diff-card pri sec) 1)    [(cube-move pri) (cube-move sec)] ; next to each other
    (= (diff-vec pri sec) [1 1]) [pri (cube-move sec)]             ; disconnected
    (= (diff-card pri sec) 2)    [pri (cube-move (cube-move sec))] ; next diag
    :else                        [pri sec]))

(defn merged?
  "Whether the cubes are currently merged, i.e. they form a cuboid."
  [pos]
  (>= 1 (apply diff-card pos)))

(defn cube-up
  "Move a cube up."
  [[x z]]
  [x (dec z)])

(defn up
  "Move the two cubes up, if merged. Otherwise, only move the first cube."
  [[pri sec :as pos]]
  (if (merged? pos)
    (re-merge [(cube-up pri) sec] cube-up)
    [(cube-up pri) sec]))

(defn cube-down
  "Move a cube down."
  [[x z]]
  [x (inc z)])

(defn down
  "Same as up, but downward."
  [[pri sec :as pos]]
  (if (merged? pos)
    (re-merge [(cube-down pri) sec] cube-down)
    [(cube-down pri) sec]))

(defn cube-left
  "Move a cube left."
  [[x z]]
  [(dec x) z])

(defn left
  "Same as up, but leftward."
  [[pri sec :as pos]]
  (if (merged? pos)
    (re-merge [(cube-left pri) sec] cube-left)
    [(cube-left pri) sec]))

(defn cube-right
  "Move a cube right."
  [[x z]]
  [(inc x) z])

(defn right
  "Same as up, but rightward."
  [[pri sec :as pos]]
  (if (merged? pos)
    (re-merge [(cube-right pri) sec] cube-right)
    [(cube-right pri) sec]))
