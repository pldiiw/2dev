(ns cuboid.scenes.high-scores
  (:require [cuboid.score :as score]
            [clojure.pprint :as pprint]))

(defn setup [{name :score/name :as state}]
  (if (nil? name)
    (assoc state
           :scene/next 'cuboid.scenes.list-input/scene
           :list-input/quit-to 'cuboid.scenes.high-scores/scene
           :list-input/instruction "Courses' high scores"
           :list-input/input-to :score/name
           :list-input/choices (score/names)
           :list-input/cancel-to 'cuboid.scenes.menu/scene)
    (-> state
        (assoc :scene/next 'cuboid.scenes.list-input/scene
               :list-input/quit-to 'cuboid.scenes.high-scores/scene
               :list-input/instruction (str "High scores for " \" name \")
               :list-input/input-to :list-input/discard-me
               :list-input/transform-input (fn [input] true)
               :list-input/choices (->> (score/load name)
                                        (score/sort-by-best)
                                        (map-indexed vector)
                                        (map (fn [[rank [player score]]] (format "%d. %s (%d)" (inc rank) player score)))
                                        (vec)))
        (dissoc :score/name))))

(def scene {:setup         `setup
            :update        'identity
            :draw          'identity})
