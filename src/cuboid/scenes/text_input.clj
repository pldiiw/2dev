(ns cuboid.scenes.text-input
  (:require [cuboid.controls.text-input :as controls.text-input]
            [cuboid.ui.text-input :as ui.text-input]))

(defn setup [state]
  (ui.text-input/setup state))

(defn update-state [state]
  state)

(defn draw [state]
  (ui.text-input/draw state))

(defn destroy [state]
  (-> state
      (ui.text-input/destroy)
      (dissoc :text-input/quit-to
              :text-input/instruction
              :text-input/input-to
              :text-input/input
              :text-input/transform-input
              :text-input/discard-me
              :text-input/cancel-to)))

(def scene {:setup         `setup
            :update        `update-state
            :draw          `draw
            :key-pressed   `controls.text-input/keypress
            :mouse-pressed `controls.text-input/click
            :destroy       `destroy})
