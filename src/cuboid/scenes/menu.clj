(ns cuboid.scenes.menu
  (:require [cuboid.ui.menu :as ui.menu]
            [cuboid.controls.menu :as controls.menu]))

(defn setup [state]
  (ui.menu/setup state))

(defn update-state [state]
  state)

(defn draw [state]
  (ui.menu/draw state))

(defn destroy [state]
  (ui.menu/destroy state))

(def scene {:setup         `setup
            :update        `update-state
            :draw          `draw
            :key-pressed   `controls.menu/keypress
            :mouse-pressed `controls.menu/click
            :destroy       `destroy})
