(ns cuboid.scenes.course-editor
  (:require [cuboid.ui.course-editor :as ui.course-editor]
            [cuboid.controls.course-editor :as controls.course-editor]
            [cuboid.course :as course]))

(defn save-course-wrapper [{save-as :course-editor/save-as :as state}]
  (if save-as
    (do
      (course/save save-as state)
      (-> state
          (assoc :course-editor/saved-as save-as)
          (dissoc :course-editor/save-as)))
    state))

(defn add-to-course-wrapper [{add-to-course :course-editor/add-to-course :as state}]
  (if add-to-course
    (-> state
        (update :course/course conj add-to-course)
        (dissoc :course-editor/add-to-course)
        (ui.course-editor/setup))
    state))

(defn dedup-course-wrapper [{course :course/course :as state}]
  (-> state
      (update :course/course (comp vec (partial reduce #(if (contains? (set %1) %2) %1 (conj %1 %2)) [])))
      (#(if (not= course (:course/course %1))
          (ui.course-editor/setup %1)
          %1))))

(defn load-course-wrapper [{course-to-load :course-editor/course-to-load :as state}]
  (if course-to-load
    (-> state
        (merge (course/load course-to-load))
        (assoc :course-editor/saved-as course-to-load)
        (dissoc :course-editor/course-to-load))
    state))

(defn setup [state]
  (-> state
      (save-course-wrapper)
      (add-to-course-wrapper)
      (dedup-course-wrapper)
      (load-course-wrapper)
      (ui.course-editor/setup)))

(defn update-state [state]
  state)

(defn draw [state]
  (ui.course-editor/draw state))

(defn destroy [state]
  (-> state
      (ui.course-editor/destroy)
      (course/destroy)
      (dissoc :course-editor/saved-as
              :course/course)))

(def scene {:setup         `setup
            :update        `update-state
            :draw          `draw
            :key-pressed   `controls.course-editor/keypress
            :mouse-pressed `controls.course-editor/click
            :destroy       `destroy})
