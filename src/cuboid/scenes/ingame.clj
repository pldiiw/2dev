(ns cuboid.scenes.ingame
  (:require [quil.core :as q]
            [cuboid.level :as level]
            [cuboid.ui.ingame :as ui.ingame]
            [cuboid.cubes :as cubes]
            [cuboid.course :as course]
            [cuboid.triggers :as triggers]
            [cuboid.controls.ingame :as controls.ingame]
            [cuboid.run :as run]
            [cuboid.score :as score]
            [clojure.data :as data]
            [cuboid.movement :as movement]))

(def MAGIC-ANGLE (q/atan (q/cos q/QUARTER-PI)))

(defn save-run-wrapper [{save-as :ingame/save-as :as state}]
  (if save-as
    (do
      (run/save save-as state)
      (-> state
          (assoc :ingame/run-to-load save-as
                 :ingame/saved-as save-as)
          (dissoc :ingame/save-as)))
    state))

(defn load-run-wrapper [{run-to-load :ingame/run-to-load :as state}]
  (if run-to-load
    (-> state
        (merge (run/load run-to-load))
        (dissoc :ingame/run-to-load))
    state))

(defn setup [state]
  (-> state
      (save-run-wrapper)
      (course/setup)
      (score/setup)
      (level/setup)
      (ui.ingame/setup)
      (load-run-wrapper)))

(defn update-state [state]
  (-> state
      (triggers/no-time-left-trigger-wrapper)))

(defn draw [{pos :level/pos lvl :level/level all-shapes :shapes/shapes :as state}]
  (q/background 255)
  (q/lights)

  (q/push-matrix)
  (q/translate (/ (q/width) 2) (/ (q/height) 2) 0)
  (q/scale (level/top-down-view-optimal-scale lvl))
  (apply q/translate (level/top-down-view-optimal-translate lvl))
  (q/rotate-x (* -0.75 q/HALF-PI))

  (level/draw lvl all-shapes)
  (cubes/draw pos (:cube all-shapes))
  (when (not (movement/merged? pos))
    (cubes/draw-arrow pos (:arrow all-shapes)))
  (q/pop-matrix)

  (ui.ingame/draw state))

(defn destroy [state]
  (-> state
      (ui.ingame/destroy)
      (level/destroy)
      (score/destroy)
      (course/destroy)
      (dissoc :ingame/quit-to
              :ingame/saved-as)))

(def scene {:setup         `setup
            :update        `update-state
            :draw          `draw
            :key-pressed   `controls.ingame/keypress
            :mouse-pressed `controls.ingame/click
            :destroy       `destroy})
