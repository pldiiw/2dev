(ns cuboid.scenes.level-editor
  (:require [cuboid.controls.level-editor :as controls.level-editor]
            [quil.core :as q]
            [cuboid.level :as level]
            [cuboid.ui.level-editor :as ui.level-editor]
            [clojure.pprint :as pprint]
            [cuboid.course :as course]))

(defn save-level-wrapper [{save-as :level-editor/save-as :as state}]
  (if save-as
    (do
      (level/save save-as state)
      (-> state
          (assoc :course/course [save-as]
                 :level-editor/saved-as save-as)
          (dissoc :level-editor/save-as)))
    state))

(defn load-tryout-wrapper [{tryout-level-name :level-editor/tryout-level-name :as state}]
  (if tryout-level-name
    (assoc state :course/course [tryout-level-name])
    state))

(defn delete-tryout-wrapper [{tryout-level-name :level-editor/tryout-level-name :as state}]
  (if tryout-level-name
    (do
      (level/delete tryout-level-name)
      (dissoc state :level-editor/tryout-level-name))
    state))

(defn setup [state]
  (-> state
      (assoc :level-editor/saved-as (first (:course/course state)))
      (save-level-wrapper)
      (load-tryout-wrapper)
      (level/setup)
      (assoc :level-editor/blocks [{:type :start}
                                   {:type :empty}
                                   {:type :brick}
                                   {:type :wood}
                                   {:type :normal}
                                   {:type :strong}
                                   {:type :teleport}
                                   {:type :finish}]
              :level-editor/edit  [0 0]
              :level-editor/undo  (:level-editor/undo state [])
              :level-editor/redo  (:level-editor/redo state '()))
      (ui.level-editor/setup)
      (delete-tryout-wrapper)))

(defn update-state [state]
  state)

(defn draw [{pos :level/pos lvl :level/level all-shapes :shapes/shapes :as state}]
  (q/background 255)

  (q/ortho)

  (q/push-matrix)
  (q/translate (/ (q/width) 2) (/ (q/height) 2) 0)
  (q/scale (level/top-down-view-optimal-scale lvl))
  (apply q/translate (level/top-down-view-optimal-translate lvl))
  (q/rotate-x (- q/HALF-PI))

  (level/draw lvl all-shapes)
  (q/pop-matrix)

  (ui.level-editor/draw state))

(defn destroy [state]
  (-> state
      (ui.level-editor/destroy)
      (level/destroy)
      (course/destroy)
      (dissoc :level-editor/saved-as
              :level-editor/blocks
              :level-editor/edit
              :level-editor/mouse-on
              :level-editor/undo
              :level-editor/redo
              :course/course)))

(def scene {:setup         `setup
            :update        `update-state
            :draw          `draw
            :key-pressed   `controls.level-editor/keypress
            :mouse-pressed `controls.level-editor/click
            :mouse-moved   `controls.level-editor/mouse-moved
            :destroy       `destroy})
