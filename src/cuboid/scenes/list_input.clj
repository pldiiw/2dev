(ns cuboid.scenes.list-input
  (:require [cuboid.controls.list-input :as controls.list-input]
            [cuboid.ui.list-input :as ui.list-input]
            [cuboid.ui.core :as ui.core]
            [clojure.pprint :as pprint]))

(defn setup [state]
  (-> state
      (update :list-input/choices #(vec (cons "[Cancel]" %1)))
      (ui.list-input/setup)))

(defn update-state [state]
  state)

(defn draw [state]
  (ui.list-input/draw state))

(defn destroy [state]
  (-> state
      (ui.list-input/destroy)
      (dissoc :list-input/quit-to
              :list-input/instruction
              :list-input/input-to
              :list-input/transform-input
              :list-input/choices
              :list-input/discard-me
              :list-input/cancel-to)))

(def scene {:setup         `setup
            :update        `update-state
            :draw          `draw
            :key-pressed   `controls.list-input/keypress
            :mouse-pressed `controls.list-input/click
            :mouse-wheel   `controls.list-input/mouse-wheel
            :destroy       `destroy})
