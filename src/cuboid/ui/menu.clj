(ns cuboid.ui.menu
  (:require [cuboid.ui.core :as ui.core]
            [quil.core :as q]))

(defn setup [state]
  (let [half-width          (/ (q/width) 2)
        half-height         (/ (q/height) 2)
        main-areas          {:play-button          {:x%        50      :y       (- half-height 140)
                                                    :width     200     :height  50
                                                    :mode      :center :button? true
                                                    :text-size 24}
                             :high-scores-button   {:x%        50      :y       (- half-height 85)
                                                    :width     200     :height  50
                                                    :mode      :center :button? true
                                                    :text-size 24}
                             :level-editor-button  {:x%        50      :y       (- half-height 30)
                                                    :width     200     :height  50
                                                    :mode      :center :button? true
                                                    :text-size 24}
                             :course-editor-button {:x%        50      :y       (+ half-height 25)
                                                    :width     200     :height  50
                                                    :mode      :center :button? true
                                                    :text-size 24}
                             :quit-button          {:x%        50      :y       (+ half-height 80)
                                                    :width     200     :height  50
                                                    :mode      :center :button? true
                                                    :text-size 24}}
        play-areas          {:new-run-button       {:x%        50      :y       (- half-height 85)
                                                    :width     400     :height  50
                                                    :mode      :center :button? true
                                                    :text-size 24}
                             :load-run-button      {:x%        50      :y       (- half-height 30)
                                                    :width     400     :height  50
                                                    :mode      :center :button? true
                                                    :text-size 24}
                             :timer-enabled-button {:x%        50      :y       (+ half-height 25)
                                                    :width     400     :height  50
                                                    :mode      :center :button? true
                                                    :text-size 24}
                             :back-button          {:x%        50      :y       (+ half-height 80)
                                                    :width     400     :height  50
                                                    :mode      :center :button? true
                                                    :text-size 24}}
        level-editor-areas  {:create-new-level-button {:x%        50      :y       (- half-height 85)
                                                       :width     300     :height  50
                                                       :mode      :center :button? true
                                                       :text-size 24}
                             :edit-level-button       {:x%        50      :y       (- half-height 30)
                                                       :width     300     :height  50
                                                       :mode      :center :button? true
                                                       :text-size 24}
                             :back-button             {:x%        50      :y       (+ half-height 25)
                                                       :width     300     :height  50
                                                       :mode      :center :button? true
                                                       :text-size 24}}
        course-editor-areas {:create-new-course-button {:x%        50      :y       (- half-height 85)
                                                        :width     300     :height  50
                                                        :mode      :center :button? true
                                                        :text-size 24}
                             :edit-course-button       {:x%        50      :y       (- half-height 30)
                                                        :width     300     :height  50
                                                        :mode      :center :button? true
                                                        :text-size 24}
                             :back-button              {:x%        50      :y       (+ half-height 25)
                                                        :width     300     :height  50
                                                        :mode      :center :button? true
                                                        :text-size 24}}]
    (-> state
        (ui.core/setup)
        (assoc :ui.menu/main-areas main-areas
               :ui.menu/play-areas play-areas
               :ui.menu/level-editor-areas level-editor-areas
               :ui.menu/course-editor-areas course-editor-areas
               :ui.menu/areas main-areas))))

(defn draw [{ui-graphics :ui/graphics areas :ui.menu/areas :as state}]
  (q/with-graphics ui-graphics
    (q/background 255)
    (q/text-size 24)
    (doseq [[name area] areas]
      (ui.core/draw-area
       (condp = name
         :play-button              "Play"
         :high-scores-button       "High Scores"
         :level-editor-button      "Level Editor"
         :course-editor-button     "Course Editor"
         :quit-button              "Quit"
         :new-run-button           "Start a new run"
         :timer-enabled-button     (str "Timer activated: " (if (:course/timer-enabled state) "Yes" "No"))
         :load-run-button          "Load a previous run"
         :create-new-level-button  "Create a new level"
         :edit-level-button        "Edit a level"
         :create-new-course-button "Create a new course"
         :edit-course-button       "Edit a course"
         :back-button              "Back")
       area)))
  (q/hint :disable-depth-test)
  (q/image ui-graphics 0 0)
  (q/hint :enable-depth-test))

(defn destroy [state]
  (-> state
      (dissoc :ui.menu/areas
              :ui.menu/main-areas
              :ui.menu/play-areas
              :ui.menu/level-editor-areas
              :ui.menu/course-editor-areas)
      (ui.core/destroy)))
