(ns cuboid.ui.ingame
  (:require [quil.core :as q]
            [cuboid.ui.core :as ui.core]
            [cuboid.level :as level]))

(defn setup [state]
  (-> state
      (ui.core/setup)
      (assoc :ui.ingame/areas
             {:score-display     {:x         60      :y       (- (q/height) 25)
                                  :width     100     :height  25
                                  :mode      :center :button? false
                                  :text-size 18}
              :time-left-display {:x         165     :y       (- (q/height) 25)
                                  :width     200     :height  25
                                  :mode      :center :button? false
                                  :text-size 18}
              :save-button       {:x         (- (q/width) 180) :y       (- (q/height) 25)
                                  :width     110               :height  30
                                  :mode      :center           :button? true
                                  :text-size 18}
              :load-button       {:x         (- (q/width) 65) :y       (- (q/height) 25)
                                  :width     110              :height  30
                                  :mode      :center          :button? true
                                  :text-size 18}
              :quit-button       {:x         (- (q/width) 60) :y       25
                                  :width     80               :height  25
                                  :mode      :center          :button? true
                                  :text-size 18}})))

(defn draw [{score :score/score timer-enabled :course/timer-enabled time :level/time started-at :level/started-at ui-graphics :ui/graphics areas :ui.ingame/areas}]
  (q/with-graphics ui-graphics
    (q/background 0 0)
    (doseq [[name area] areas]
      (ui.core/draw-area
       (condp = name
         :score-display     (str "Score: " score)
         :time-left-display (if timer-enabled
                              (format "%.2fs left" (double (/ (level/time-left started-at time) 1000)))
                              "")
         :save-button       "Save game"
         :load-button       "Load game"
         :quit-button       "Quit")
       area)))
  (q/hint :disable-depth-test)
  (q/image ui-graphics 0 0)
  (q/hint :enable-depth-test))

(defn destroy [state]
  (-> state
      (dissoc :ui.ingame/areas)
      (ui.core/destroy)))
