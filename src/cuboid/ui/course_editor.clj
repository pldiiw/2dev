(ns cuboid.ui.course-editor
  (:require [cuboid.ui.core :as ui.core]
            [quil.core :as q]))

(defn setup [state]
  (q/text-size 18) ; for q/text-width
  (let [half-width  (/ (q/width) 2)
        half-height (/ (q/height) 2)
        course      (conj (:course/course state) "+")]
    (-> state
        (ui.core/setup)
        (assoc :ui.course-editor/areas
               (merge {:save-button {:x         (- half-width 105) :y       (- (q/height) 25)
                                     :width     200                :height  25
                                     :mode      :center            :button? true
                                     :text-size 18}
                       :quit-button {:x         (+ half-width 105) :y       (- (q/height) 25)
                                     :width     200                :height  25
                                     :mode      :center            :button? true
                                     :text-size 18}}
                      (into
                       {}
                       (reduce
                        (fn [acc level-name]
                          (conj acc (if (empty? acc)
                                      [level-name {:x         25
                                                   :y         25
                                                   :width     (+ 50 (q/text-width level-name))
                                                   :height    50
                                                   :mode      :corner
                                                   :button?   true
                                                   :text-size 18}]
                                      (let [[_ {old-x      :x
                                                old-y      :y
                                                old-width  :width
                                                old-height :height}] (last acc)
                                            height                   50
                                            width                    (+ 50 (q/text-width level-name))
                                            greater-than-width?      (> (+ old-x old-width 15 width) (- (q/width) 25))
                                            x                        (if greater-than-width? 25 (+ old-x old-width 15))
                                            y                        (if greater-than-width? (+ old-y 65) old-y)]
                                        [level-name {:x         x
                                                     :y         y
                                                     :width     width
                                                     :height    height
                                                     :mode      :corner
                                                     :button?   true
                                                     :text-size 18}]))))
                        []
                        course)))))))

(defn draw [{ui-graphics :ui/graphics areas :ui.course-editor/areas course :course/course :as state}]
  (q/with-graphics ui-graphics
    (q/background 255)
    (doseq [[name area] areas]
      (ui.core/draw-area (condp = name
                           :save-button "Save course"
                           :quit-button "Quit"
                           name)
                         area)))
  (q/image ui-graphics 0 0))

(defn destroy [state]
  (-> state
      (dissoc :ui.course-editor/areas)
      (ui.core/destroy)))
