(ns cuboid.ui.level-editor
  (:require [cuboid.ui.core :as ui.core]
            [clojure.string :as string]
            [quil.core :as q]
            [cuboid.level :as level]))

(defn setup [state]
  (-> state
      (ui.core/setup)
      (assoc :ui.level-editor/areas
             {:selected-block-button              {:x         110     :y       (- (q/height) 25)
                                                   :width     200     :height  25
                                                   :mode      :center :button? true
                                                   :text-size 18}
              :remove-second-to-timer-button      {:x         240     :y       (- (q/height) 25)
                                                   :width     40      :height  25
                                                   :mode      :center :button? true
                                                   :text-size 18}
              :remove-half-second-to-timer-button {:x         280     :y       (- (q/height) 25)
                                                   :width     40      :height  25
                                                   :mode      :center :button? true
                                                   :text-size 18}
              :timer-display                      {:x         360     :y       (- (q/height) 25)
                                                   :width     120     :height  25
                                                   :mode      :center :button? false
                                                   :text-size 18}
              :add-half-second-to-timer-button    {:x         440     :y       (- (q/height) 25)
                                                   :width     40      :height  25
                                                   :mode      :center :button? true
                                                   :text-size 18}
              :add-second-to-timer-button         {:x         480     :y       (- (q/height) 25)
                                                   :width     40      :height  25
                                                   :mode      :center :button? true
                                                   :text-size 18}
              :undo-button                        {:x         550     :y       (- (q/height) 25)
                                                   :width     60      :height  25
                                                   :mode      :center :button? true
                                                   :text-size 18}
              :redo-button                        {:x         620     :y       (- (q/height) 25)
                                                   :width     60      :height  25
                                                   :mode      :center :button? true
                                                   :text-size 18}
              :try-out-level-button               {:x         (- (q/width) 85) :y       (- (q/height) 25)
                                                   :width     150              :height  25
                                                   :mode      :center          :button? true
                                                   :text-size 18}
              :save-level-button                  {:x         (- (q/width) 60) :y       60
                                                   :width     100              :height  25
                                                   :mode      :center          :button? true
                                                   :text-size 18}
              :go-back-to-main-menu-button        {:x         (- (q/width) 60) :y       25
                                                   :width     80               :height  25
                                                   :mode      :center          :button? true
                                                   :text-size 18}})))

(defn draw-selected [[x y :as selected]]
  (when (not (nil? selected))
    (q/no-fill)
    (q/stroke 255 0 0)
    (q/stroke-weight 0.1)
    (q/rect-mode :corner)
    (q/rect x y 1 1)))

(defn draw-links [lvl]
  (q/fill 0)
  (q/stroke 0 155 0)
  (q/stroke-weight 0.1)
  (q/rect-mode :center)
  (doseq [[row-index row]   (level/memoized-indexed lvl)
          [col-index block] row
          [to-col to-row]   (filter
                             (complement empty?)
                             (apply conj (:to block) (map (fn [{x :x z :z}] [x z]) (:toggles block))))
          :when             (and to-row to-col)]
    (let [link-line (vec (map #(+ 0.5 %1) [col-index row-index to-col to-row]))
          rect-x    (link-line 2)
          rect-y    (link-line 3)
          rect-side 0.25]
      (apply q/line link-line)
      (q/rect rect-x rect-y rect-side rect-side))))

(defn draw-mouse-on [[x y :as mouse-on]]
  (when mouse-on
    (q/no-fill)
    (q/stroke 100)
    (q/stroke-weight 0.1)
    (q/rect-mode :corner)
    (q/rect x y 1 1)))

(defn draw-start-overlay [lvl]
  (q/no-fill)
  (q/stroke 0)
  (q/stroke-weight 0.05)
  (when-let [[[start-x start-y] _] (level/start-position lvl)]
    (q/line start-x start-y (inc start-x) (inc start-y))
    (q/line (inc start-x) start-y start-x (inc start-y))))

(defn draw [{ui-graphics :ui/graphics areas :ui.level-editor/areas blocks :level-editor/blocks lvl :level/level selected :level-editor/selected time :level/time mouse-on :level-editor/mouse-on :as state}]
  (let [[ox oy _]     (level/top-down-view-optimal-translate lvl)
        optimal-scale (level/top-down-view-optimal-scale lvl)
        block         (first blocks)]
    (q/with-graphics ui-graphics
      (q/background 0 0)
      (q/rect-mode :center)
      (q/push-matrix)

      (q/translate (/ (q/width) 2) (/ (q/height) 2))
      (q/scale optimal-scale)
      (q/with-translation [ox oy]
        (q/push-style)
        (draw-start-overlay lvl)
        (draw-selected selected)
        (draw-links lvl)
        (draw-mouse-on mouse-on)
        (q/pop-style))
      (q/pop-matrix)

      (doseq [[name area] areas]
        (ui.core/draw-area
         (condp = name
           :selected-block-button              (str "Selected: "
                                                    (string/capitalize
                                                     (subs (str (:type (first blocks))) 1)))
           :add-second-to-timer-button         "++"
           :add-half-second-to-timer-button    "+"
           :timer-display                      (str "Time: " time)
           :remove-half-second-to-timer-button "-"
           :remove-second-to-timer-button      "--"
           :undo-button                        "Undo"
           :redo-button                        "Redo"
           :try-out-level-button               "Try out level"
           :save-level-button                  "Save level"
           :go-back-to-main-menu-button        "Quit")
         area))))
  (q/hint :disable-depth-test)
  (q/image ui-graphics 0 0)
  (q/hint :enable-depth-test))

(defn destroy [state]
  (-> state
      (dissoc :ui.level-editor/areas)
      (ui.core/destroy)))
