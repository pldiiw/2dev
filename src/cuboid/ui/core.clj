(ns cuboid.ui.core
  (:require [quil.core :as q]))

(defn setup [state]
  (merge state
         {:ui/graphics (q/create-graphics (q/width) (q/height) :p2d)}))

(defn destroy [state]
  (dissoc state :ui/graphics))

(defn in-what [areas click-x click-y]
  (reduce-kv
   (fn [_ area {:keys [x y width height x% y% width% height% mode]}]
     (let [x (or x (* (/ x% 100) (q/width)))
           y (or y (* (/ y% 100) (q/height)))
           width (or width (* (/ width% 100) (q/width)))
           height (or height (* (/ height% 100) (q/height)))]
       (when
          (condp = mode
            :center (and (< (- x (/ width 2)) click-x (+ x (/ width 2)))
                         (< (- y (/ height 2)) click-y (+ y (/ height 2))))
            :corner (and (< x click-x (+ x width)) (< y click-y (+ y height))))
        (reduced area))))
   nil
   areas))


(defn draw-area [s {:keys [x y x% y% width height width% height% mode button? text-size] :as area}]
  (let [x (or x (* (/ x% 100) (q/width)))
        y (or y (* (/ y% 100) (q/height)))
        width (or width (* (/ width% 100) (q/width)))
        height (or height (* (/ height% 100) (q/height)))]
    (q/rect-mode mode)
    (when button?
      (q/stroke 0)
      (q/fill 255)
      (q/rect x y width height))
    (q/stroke 255)
    (q/fill 0)
    (q/text-align :center :center)
    (q/text-size text-size)
    (q/text s x y width height)))
