(ns cuboid.ui.list-input
  (:require [cuboid.ui.core :as ui.core]
            [quil.core :as q]))

(defn setup [state]
  (let [half-width  (/ (q/width) 2)
        half-height (/ (q/height) 2)]
    (-> state
        (ui.core/setup)
        (assoc :ui.list-input/areas {:instruction-display {:x         half-width :y       25
                                                           :width     (q/width)  :height  50
                                                           :mode      :center    :button? false
                                                           :text-size 24}}))))

(defn draw [{ui-graphics :ui/graphics areas :ui.list-input/areas instruction :list-input/instruction input :list-input/input choices :list-input/choices :as state}]
  (q/with-graphics ui-graphics
    (q/background 255)
    (q/rect-mode :center)
    (q/stroke 255)
    (q/fill 0)
    (ui.core/draw-area instruction (:instruction-display areas))
    (let [max-text-width (+ 50 (apply max (map q/text-width choices)))]
      (doseq [i    (range (count choices))
              :let [choice (choices i)
                    area {:x       (/ (q/width) 2)
                          :y       (+ 75 (* 55 i))
                          :width   max-text-width
                          :height  50
                          :mode    :center
                          :button? true
                          :text-size 24}]]
        (ui.core/draw-area choice area))))
  (q/image ui-graphics 0 0))


(defn destroy [state]
  (-> state
      (dissoc :ui.list-input/areas)
      (ui.core/destroy)))
