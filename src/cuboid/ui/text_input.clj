(ns cuboid.ui.text-input
  (:require [cuboid.ui.core :as ui.core]
            [quil.core :as q]))

(defn setup [state]
  (let [half-width  (/ (q/width) 2)
        half-height (/ (q/height) 2)]
    (-> state
        (ui.core/setup)
        (assoc :ui.text-input/areas
               {:instruction-display   {:x         half-width :y       (- half-height 150)
                                        :width     (q/width)  :height  150
                                        :mode      :center    :button? false
                                        :text-size 24}
                :current-input-display {:x         half-width :y       half-height
                                        :width     (q/width)  :height  50
                                        :mode      :center    :button? false
                                        :text-size 24}
                :submit-button         {:x         (- half-width 105) :y       (+ half-height 100)
                                        :width     200                :height  50
                                        :mode      :center            :button? true
                                        :text-size 24}
                :cancel-button         {:x         (+ half-width 105) :y       (+ half-height 100)
                                        :width     200                :height  50
                                        :mode      :center            :button? true
                                        :text-size 24}}))))

(defn draw [{ui-graphics :ui/graphics areas :ui.text-input/areas instruction :text-input/instruction input :text-input/input :as state}]
  (q/with-graphics ui-graphics
    (q/background 255)
    (q/text-size 24)
    (doseq [[name area] areas]
      (ui.core/draw-area (condp = name
                           :instruction-display instruction
                           :current-input-display (or input "")
                           :submit-button "Submit"
                           :cancel-button "Cancel"
                           name)
                         area)))
  (q/image ui-graphics 0 0))

(defn destroy [state]
  (-> state
      (dissoc :ui.text-input/areas)
      (ui.core/destroy)))
