(ns cuboid.controls.level-editor
  (:require [clojure.pprint :as pprint]
            [cuboid.ui.core :as ui.core]
            [cuboid.ui.level-editor :as ui.level-editor]
            [quil.core :as q]
            [cuboid.level :as level]
            [clojure.edn :as edn]
            [cuboid.shapes :as shapes]))

(defmulti keypress (fn [state event] (:key event (:key-code event))))

(defmethod keypress (keyword " ")
  ;; "Space, change selected block, i.e. the block that is added when clicking on a cell"
  [{[selected-block & other-blocks] :level-editor/blocks :as state} event]
  (assoc state :level-editor/blocks (conj (vec other-blocks) selected-block)))

(defmethod keypress :p
  ;; "Persist/save level"
  [{lvl :level/level time :level/time saved-as :level-editor/saved-as :as state} event]
  (assoc state
         :scene/next 'cuboid.scenes.text-input/scene
         :scene/keep [:level/level :level/time]
         :text-input/quit-to 'cuboid.scenes.level-editor/scene
         :text-input/instruction "Enter a name for your level"
         :text-input/input-to :level-editor/save-as
         :text-input/input saved-as))

(defmethod keypress :t
  ;; "Tryout level"
  [state event]
  (let [rand-level-name (str (java.util.UUID/randomUUID))]
    (level/save rand-level-name state)
    (assoc state
           :scene/next 'cuboid.scenes.ingame/scene
           :scene/keep [:course/course :course/looping :level-editor/tryout-level-name]
           :ingame/quit-to 'cuboid.scenes.level-editor/scene
           :course/course [rand-level-name]
           :course/looping true
           :level-editor/tryout-level-name rand-level-name)))

(defmethod keypress :i
  ;; "Print out the current state of the game"
  [state event]
  (pprint/pprint state)
  state)

(defmethod keypress :o
  ;; "Reload UI"
  [state event]
  (ui.level-editor/setup state))

(defmethod keypress :g
  ;; "Reload shapes"
  [state event]
  (shapes/setup state))

(defmethod keypress :q
  ;; "Quit to main menu"
  [state event]
  (-> state
      (assoc :scene/next 'cuboid.scenes.menu/scene)))

(defmethod keypress :u
  ;; "Undo, go back to previous level snapshot"
  [{lvl :level/level undo :level-editor/undo :as state} event]
  (if (not-empty undo)
    (-> state
        (update :level-editor/redo conj lvl)
        (assoc :level/level (last undo))
        (update :level-editor/undo (comp vec butlast)))
    state))

(defmethod keypress :r
  ;; "Redo, forward to next level snapshot"
  [{lvl :level/level redo :level-editor/redo :as state} event]
  (if (not-empty redo)
    (-> state
        (update :level-editor/undo conj lvl)
        (assoc :level/level (first redo))
        (update :level-editor/redo rest))
    state))

(defmethod keypress :default [state event]
  state)

(defmulti click (fn [{areas :ui.level-editor/areas} {:keys [x y button]}] [button (ui.core/in-what areas x y)]))

(defn shift-selected [[sx sz :as selected] x z]
  (if (nil? selected)
    nil
    [(+ sx x) (+ sz z)]))

(defn link [{:keys [type to toggles] :as block} target-type x z]
  (if (= type :teleport)
    (assoc block :to [[x z] (first to)])
    (assoc block :toggles (conj toggles {:x x :z z :to {:type (if (= target-type :empty) :brick :empty)}}))))

(defn unlink [{:keys [type to toggles] :as block} x z]
  (if (#{:normal :strong} type)
    (assoc block :toggles (filter #(or (not= (:x %1) x)
                                       (not= (:z %1) z))
                                  toggles))
    block))

(defmethod click [:left nil] [{blocks :level-editor/blocks level :level/level [sx sy :as selected] :level-editor/selected :as state} {:keys [x y] :as event}]
  (let [half-width              (/ (q/width) 2)
        half-height             (/ (q/height) 2)
        [ox oy _]               (level/top-down-view-optimal-translate level)
        optimal-scale           (level/top-down-view-optimal-scale level)
        sox                     (* ox optimal-scale)
        soy                     (* oy optimal-scale)
        col                     (q/floor (/ (- x half-width sox) optimal-scale))
        row                     (q/floor (/ (- y half-height soy) optimal-scale))
        abs-neg-col             (if (neg? col) (Math/abs col) 0)
        abs-neg-row             (if (neg? row) (Math/abs row) 0)
        shifted-col             (+ col abs-neg-col)
        shifted-row             (+ row abs-neg-row)
        shifted-selected        (when selected (shift-selected selected abs-neg-col abs-neg-row))
        [shifted-sx shifted-sy] shifted-selected
        block                   (first blocks)
        clicked-block           (get-in level [row col])
        state                   (-> state
                                    (update :level-editor/undo conj level)
                                    (update :level/level #(level/fill-to %1 col row))
                                    (update :level/level #(level/shift-links %1 abs-neg-col abs-neg-row))
                                    (assoc :level-editor/selected shifted-selected))]
    (cond
      (not (nil? selected))
      (update-in state
                 [:level/level shifted-sy shifted-sx]
                 (fn [{toggles :toggles :as selected-block}]
                   (let [type (if (= [col row] [shifted-col shifted-row])
                                (:type clicked-block :empty)
                                :empty)]
                     (if (empty? (filter
                                  (fn [{x :x z :z}] (= [x z] [shifted-col shifted-row]))
                                  toggles))
                       (link selected-block type shifted-col shifted-row)
                       (unlink selected-block shifted-col shifted-row)))))

      :default
      (assoc-in state
                [:level/level shifted-row shifted-col]
                (if (= (:type block) :teleport)
                  (assoc block :to [[col row] [col row]])
                  block)))))

(defmethod click [:right nil] [{level :level/level selected :level-editor/selected :as state} {:keys [x y] :as event}]
  (let [half-width    (/ (q/width) 2)
        half-height   (/ (q/height) 2)
        [ox oy _]     (level/top-down-view-optimal-translate level)
        optimal-scale (level/top-down-view-optimal-scale level)
        sox           (* ox optimal-scale)
        soy           (* oy optimal-scale)
        col           (q/floor (/ (- x half-width sox) optimal-scale))
        row           (q/floor (/ (- y half-height soy) optimal-scale))
        clicked-block (get-in level [row col])]
    (cond
      (= selected [col row])
      (assoc state :level-editor/selected nil)

      (#{:normal :strong :teleport} (:type clicked-block))
      (assoc state :level-editor/selected [col row])

      :default
      (assoc state :level-editor/selected nil))))

(defmethod click [:left :selected-block-button] [state event]
  (keypress state {:key (keyword " ")}))

(defmethod click [:left :add-second-to-timer-button] [state event]
  (update state :level/time + 1000))

(defmethod click [:left :add-half-second-to-timer-button] [state event]
  (update state :level/time + 500))

(defmethod click [:left :remove-second-to-timer-button] [state event]
  (if (<= (:level/time state) 500)
    state
    (update state :level/time + -1000)))

(defmethod click [:left :remove-half-second-to-timer-button] [state event]
  (if (zero? (:level/time state))
    state
    (update state :level/time + -500)))

(defmethod click [:left :try-out-level-button] [state event]
  (keypress state {:key :t}))

(defmethod click [:left :save-level-button] [state event]
  (keypress state {:key :p}))

(defmethod click [:left :go-back-to-main-menu-button] [state event]
  (keypress state {:key :q}))

(defmethod click [:left :undo-button] [state event]
  (keypress state {:key :u}))

(defmethod click [:left :redo-button] [state event]
  (keypress state {:key :r}))

(defmethod click :default [state event]
  state)

(defn mouse-moved [{areas :ui.level-editor/areas level :level/level :as state} {x :x y :y}]
  (assoc state
         :level-editor/mouse-on
         (if (nil? (ui.core/in-what areas x y))
           (let [half-width    (/ (q/width) 2)
                 half-height   (/ (q/height) 2)
                 [ox oy _]     (level/top-down-view-optimal-translate level)
                 optimal-scale (level/top-down-view-optimal-scale level)
                 col           (q/floor (/ (- x half-width (* ox optimal-scale)) optimal-scale))
                 row           (q/floor (/ (- y half-height (* oy optimal-scale)) optimal-scale))]
             [col row])
           nil)))
