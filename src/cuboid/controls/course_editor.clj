(ns cuboid.controls.course-editor
  (:require [quil.core :as q]
            [cuboid.ui.core :as ui.core]
            [clojure.pprint :as pprint]
            [cuboid.course :as course]
            [cuboid.level :as level]))

(defmulti keypress (fn [state event] (:key event (:key-code event))))

(defmethod keypress :i [state event]
  ;; "Print out the current state of the game"
  (pprint/pprint state)
  state)

(defmethod keypress :default [state event]
  state)


(defmulti click (fn [{areas :ui.course-editor/areas} {:keys [x y button]}] [button (ui.core/in-what areas x y)]))

(defmethod click [:left :save-button] [state event]
  (assoc state
         :scene/next 'cuboid.scenes.text-input/scene
         :scene/keep [:course/course]
         :text-input/quit-to 'cuboid.scenes.course-editor/scene
         :text-input/instruction "Enter the course name"
         :text-input/input-to :course-editor/save-as
         :text-input/input (:course-editor/saved-as state)))

(defmethod click [:left :quit-button] [state event]
  (assoc state :scene/next 'cuboid.scenes.menu/scene))

(defmethod click :default [{course :course/course areas :ui.course-editor/areas :as state} {x :x y :y}]
  (let [what (ui.core/in-what areas x y)]
    (prn what (contains? (set course) what) course)
    (cond
      (= what "+")
      (assoc state
             :scene/next 'cuboid.scenes.list-input/scene
             :scene/keep [:course/course]
             :list-input/quit-to 'cuboid.scenes.course-editor/scene
             :list-input/instruction "Choose a level to add to the course"
             :list-input/input-to :course-editor/add-to-course
             :list-input/choices (level/names))
      (contains? (set course) what)
      (assoc state
             :scene/next 'cuboid.scenes.list-input/scene
             :scene/keep [:course/course]
             :list-input/quit-to 'cuboid.scenes.course-editor/scene
             :list-input/instruction (str "Choose which level should replace " \" what \")
             :list-input/input-to [:course/course (.indexOf course what)]
             :list-input/choices (level/names))
      :default
      state)))
