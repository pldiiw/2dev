(ns cuboid.controls.menu
  (:require [quil.core :as q]
            [cuboid.ui.core :as ui.core]
            [clojure.pprint :as pprint]
            [cuboid.course :as course]
            [cuboid.run :as run]
            [cuboid.level :as level]))

(defmulti keypress (fn [state event] (:key event (:key-code event))))

(defmethod keypress :i [state event]
  ;; "Print out the current state of the game"
  (pprint/pprint state)
  state)

(defmethod keypress :default [state event]
  state)


(defmulti click (fn [{areas :ui.menu/areas} {:keys [x y button]}] [button (ui.core/in-what areas x y)]))

(defmethod click [:left :play-button] [state event]
  (assoc state :ui.menu/areas (:ui.menu/play-areas state)))

(defmethod click [:left :high-scores-button] [state event]
  (assoc state :scene/next 'cuboid.scenes.high-scores/scene))

(defmethod click [:left :level-editor-button] [state event]
  (assoc state :ui.menu/areas (:ui.menu/level-editor-areas state)))

(defmethod click [:left :course-editor-button] [state event]
  (assoc state :ui.menu/areas (:ui.menu/course-editor-areas state)))

(defmethod click [:left :quit-button] [state event]
  (q/exit)
  (System/exit 0))

(defmethod click [:left :new-run-button] [state event]
  (assoc state
         :scene/next 'cuboid.scenes.list-input/scene
         :scene/keep [:course/timer-enabled]
         :list-input/quit-to 'cuboid.scenes.ingame/scene
         :list-input/instruction "Choose a course to play"
         :list-input/input-to :course/name
         :list-input/choices (course/names)
         :list-input/cancel-to 'cuboid.scenes.menu/scene))

(defmethod click [:left :load-run-button] [state event]
  (assoc state
         :scene/next 'cuboid.scenes.list-input/scene
         :list-input/quit-to 'cuboid.scenes.ingame/scene
         :list-input/instruction "Choose a saved run to play"
         :list-input/input-to :ingame/run-to-load
         :list-input/choices (run/names)
         :list-input/cancel-to 'cuboid.scenes.menu/scene))

(defmethod click [:left :timer-enabled-button] [state event]
  (update state :course/timer-enabled not))

(defmethod click [:left :create-new-level-button] [state event]
  (assoc state
         :scene/next 'cuboid.scenes.level-editor/scene
         :course/course ["empty"]))

(defmethod click [:left :edit-level-button] [state event]
  (assoc state
         :scene/next 'cuboid.scenes.list-input/scene
         :list-input/quit-to 'cuboid.scenes.level-editor/scene
         :list-input/instruction "Select a level to edit"
         :list-input/input-to :course/course
         :list-input/transform-input #(conj [] %1)
         :list-input/choices (level/names)
         :list-input/cancel-to 'cuboid.scenes.menu/scene))

(defmethod click [:left :create-new-course-button] [state event]
  (assoc state :scene/next 'cuboid.scenes.course-editor/scene))

(defmethod click [:left :edit-course-button] [state event]
  (assoc state
         :scene/next 'cuboid.scenes.list-input/scene
         :list-input/quit-to 'cuboid.scenes.course-editor/scene
         :list-input/instruction "Select a course to edit"
         :list-input/input-to :course-editor/course-to-load
         :list-input/choices (course/names)
         :list-input/cancel-to 'cuboid.scenes.menu/scene))

(defmethod click [:left :back-button] [state event]
  (assoc state :ui.menu/areas (:ui.menu/main-areas state)))

(defmethod click :default [state event]
  state)
