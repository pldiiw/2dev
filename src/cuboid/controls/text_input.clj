(ns cuboid.controls.text-input
  (:require [quil.core :as q]
            [cuboid.ui.core :as ui.core]
            [clojure.pprint :as pprint]
            [clojure.string :as string]))

(def supported-characters (str "0123456789"
                               "abcdefghijklmnopqrstuvwxyz"
                               "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                               "._- "))

(defmulti keypress (fn [state event] (:key event (:key-code event))))

(defmethod keypress (keyword (str \backspace)) [state event]
  (update state :text-input/input #(string/join "" (drop-last %1))))

(defmethod keypress (keyword (str \newline))
  [{quit-to :text-input/quit-to input-to :text-input/input-to input :text-input/input transform-input :text-input/transform-input :as state} event]
  (-> state
      (assoc :scene/next quit-to)
      (assoc input-to ((or transform-input identity) input))))

(defmacro keypress-multiple [keys params & body]
  `(doseq [key# ~keys]
     (eval (defmethod keypress key# ~params ~@body))))

(keypress-multiple (map (comp keyword str) supported-characters)
  [state event]
  (update state :text-input/input #(str %1 (:raw-key event))))

(defmethod keypress :default [state event]
  state)

(defmulti click (fn [{areas :ui.text-input/areas} {:keys [x y button]}] [button (ui.core/in-what areas x y)]))

(defmethod click [:left :submit-button] [state event]
  (keypress state {:key (keyword (str \newline))}))

(defmethod click [:left :cancel-button]
  [{cancel-to :text-input/cancel-to quit-to :text-input/quit-to :as state} event]
  (assoc state :scene/next (or cancel-to quit-to)))

(defmethod click :default [state event]
  state)
