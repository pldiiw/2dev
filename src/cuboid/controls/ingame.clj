(ns cuboid.controls.ingame
  (:require [clojure.data :as data]
            [clojure.edn :as edn]
            [clojure.pprint :as pprint]
            [cuboid.movement :as movement]
            [cuboid.shapes :as shapes]
            [cuboid.course :as course]
            [cuboid.ui.core :as ui.core]
            [cuboid.triggers :as triggers]
            [cuboid.level :as level]


            [quil.core :as q]
            [cuboid.ui.ingame :as ui.ingame]
            [cuboid.run :as run]))

(defn generic-movement-keypress [movement-fn state event]
  (-> state
      (update :level/pos movement-fn)
      (update :score/score inc)
      (triggers/action-block-triggers)))

(defmulti keypress (fn [state event] (:key event (:key-code event))))

(defmethod keypress :up [state event]
  (generic-movement-keypress movement/up state event))

(defmethod keypress :down [state event]
  (generic-movement-keypress movement/down state event))

(defmethod keypress :left [state event]
  (generic-movement-keypress movement/left state event))

(defmethod keypress :right [state event]
  (generic-movement-keypress movement/right state event))

(defmethod keypress (keyword " ")
  ;; "Space key, to change the selected cube"
  [state event]
  (update state :level/pos reverse))

(defmethod keypress :t [state event]
  ;; "Toggle the timer/countdown"
  (assoc (course/reset state)
         :course/timer-enabled (not (:course/timer-enabled state))))

;; Support for zqsd
(defmethod keypress :z [state event]
  (keypress state {:key :up}))

(defmethod keypress :s [state event]
  (keypress state {:key :down}))

(defmethod keypress :q [state event]
  (keypress state {:key :left}))

(defmethod keypress :d [state event]
  (keypress state {:key :right}))

;; Saving and loading
(defmethod keypress :p
  ;; "Persist/Save run"
  [state event]
  (assoc state
         :scene/next 'cuboid.scenes.text-input/scene
         :scene/keep [:level/level :level/time :level/pos :course/name :course/looping :course/course :course/timer-enabled :score/score :level/started-at]
         :text-input/quit-to 'cuboid.scenes.ingame/scene
         :text-input/instruction "Enter a name for this save"
         :text-input/input-to :ingame/save-as
         :text-input/input (:ingame/saved-as state)))

(defmethod keypress :l
  ;; "Load run"
  [state event]
  (assoc state
         :scene/next 'cuboid.scenes.list-input/scene
         :list-input/quit-to 'cuboid.scenes.ingame/scene
         :list-input/instruction "Choose a save to load"
         :list-input/input-to :ingame/run-to-load
         :list-input/choices (run/names)))

;; Debugging-related keybindings
(defmethod keypress :r
  ;; "Reload the shapes"
  [state event]
  (shapes/setup state))

(defmethod keypress :i [state event]
  ;; "Print out the current state of the game"
  (pprint/pprint state)
  state)

(defmethod keypress :u [state event]
  ;; "Reload the UI"
  (ui.ingame/setup state))

(defmethod keypress :default [state event]
  state)


(defmulti click (fn [{areas :ui.ingame/areas} {:keys [x y button]}] [button (ui.core/in-what areas x y)]))

(defmethod click [:left :save-button] [state event]
  (keypress state {:key :p}))

(defmethod click [:left :load-button] [state event]
  (keypress state {:key :l}))

(defmethod click [:left :quit-button] [state event]
  (assoc state
         :scene/next (:ingame/quit-to state 'cuboid.scenes.menu/scene)))

(defmethod click :default [state event]
  state)
