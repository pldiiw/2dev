(ns cuboid.controls.list-input
  (:require [quil.core :as q]
            [cuboid.ui.core :as ui.core]
            [clojure.pprint :as pprint]
            [clojure.string :as string]))

(defmulti keypress (fn [state event] (:key event (:key-code event))))

(defmethod keypress :default [state event]
  state)

(defn click [{choices :list-input/choices :as state} event]
  (if-let [what (ui.core/in-what (reduce-kv
                                  #(assoc %1 %3 {:x       (/ (q/width) 2)
                                                 :y       (+ 75 (* 55 %2))
                                                 :width   (+ 50 (apply max (map q/text-width choices)))
                                                 :height  50
                                                 :mode    :center
                                                 :button? true})
                                  {}
                                  choices)
                                 (:x event)
                                 (:y event))]
    (if (= what "[Cancel]")
      (assoc state :scene/next (or (:list-input/cancel-to state) (:list-input/quit-to state)))

      (-> state
          (assoc-in (flatten [(:list-input/input-to state)]) ((:list-input/transform-input state identity) what))
          (assoc :scene/next (:list-input/quit-to state))))
    state))

(defn mouse-wheel [state event]
  (if (= event 1)
    (update state :list-input/choices #(conj (vec (rest %1)) (first %1)))
    (update state :list-input/choices #(vec (conj (butlast %1) (last %1))))))
