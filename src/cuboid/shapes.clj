(ns cuboid.shapes
  "This namespace provides all 3D models that the game uses."
  (:require [quil.core :as q]))

(defn box [width height depth [r g b]]
  (let [s (.createShape (q/current-graphics))]
    (doto s
      (.beginShape processing.core.PConstants/QUADS)
      (.fill r g b)
      (.vertex 0     0      0)
      (.vertex width 0      0)
      (.vertex width 0      depth)
      (.vertex 0     0      depth)

      (.vertex 0     height 0)
      (.vertex width height 0)
      (.vertex width height depth)
      (.vertex 0     height depth)

      (.vertex 0     0      0)
      (.vertex 0     height 0)
      (.vertex width height 0)
      (.vertex width 0      0)

      (.vertex 0     0      0)
      (.vertex 0     height 0)
      (.vertex 0     height depth)
      (.vertex 0     0      depth)

      (.vertex 0     0      depth)
      (.vertex 0     height depth)
      (.vertex width height depth)
      (.vertex width 0      depth)

      (.vertex width 0      depth)
      (.vertex width height depth)
      (.vertex width height 0)
      (.vertex width 0      0)
      (.endShape))))

(defn cuboid-shape []
  (box 1 2 1 [255 0 0]))

(defn cube-shape []
  (box 1 1 1 [255 0 0]))

(defn brick-shape []
  (box 1 0.5 1 [105 105 105]))

(defn wood-shape []
  (box 1 0.2 1 [139 69 19]))

(defn normal-shape []
  (let [s (.createShape (q/current-graphics))]
  (doto s
    (.beginShape processing.core.PConstants/QUADS)
    (.fill 0 255 0)
    (.vertex 0     0      0)
    (.vertex 1 0      0)
    (.vertex 1 0      1)
    (.vertex 0     0      1)

    (.vertex 0     0.75 0)
    (.vertex 1 0.75 0)
    (.vertex 1 0.75 1)
    (.vertex 0     0.75 1)

    (.vertex 0     0      0)
    (.vertex 0     0.75 0)
    (.vertex 1 0.75 0)
    (.vertex 1 0      0)

    (.vertex 0     0      0)
    (.vertex 0     0.75 0)
    (.vertex 0     0.75 1)
    (.vertex 0     0      1)

    (.vertex 0     0      1)
    (.vertex 0     0.75 1)
    (.vertex 1 0.75 1)
    (.vertex 1 0      1)

    (.vertex 1 0      1)
    (.vertex 1 0.75 1)
    (.vertex 1 0.75 0)
    (.vertex 1 0      0)
    
    (.vertex 0.25 0  0.25) 
    (.vertex 0.25 0  0.75) 
    (.vertex 0.75 0  0.75) 
    (.vertex 0.75 0  0.25) 
        
    (.endShape)
    )))

(defn strong-shape []
  (let [s (.createShape (q/current-graphics))]
  (doto s
    (.beginShape processing.core.PConstants/QUADS)
    (.fill 0 127 0)
    (.vertex 0     0      0)
    (.vertex 1 0      0)
    (.vertex 1 0      1)
    (.vertex 0     0      1)

    (.vertex 0     1.25 0)
    (.vertex 1 1.25 0)
    (.vertex 1 1.25 1)
    (.vertex 0     1.25 1)

    (.vertex 0     0      0)
    (.vertex 0     1.25 0)
    (.vertex 1 1.25 0)
    (.vertex 1 0      0)

    (.vertex 0     0      0)
    (.vertex 0     1.25 0)
    (.vertex 0     1.25 1)
    (.vertex 0     0      1)

    (.vertex 0     0      1)
    (.vertex 0     1.25 1)
    (.vertex 1 1.25 1)
    (.vertex 1 0      1)

    (.vertex 1 0      1)
    (.vertex 1 1.25 1)
    (.vertex 1 1.25 0)
    (.vertex 1 0      0)
    
    (.vertex 0.1 0  0.2) 
    (.vertex 0.2 0  0.1) 
    (.vertex 0.45 0  0.35) 
    (.vertex 0.35 0  0.45) 
          
    (.vertex 0.1 0  0.7) 
    (.vertex 0.2 0  0.8) 
    (.vertex 0.45 0  0.55) 
    (.vertex 0.35 0  0.45) 
    
    (.vertex 0.7 0  0.1) 
    (.vertex 0.8 0  0.2) 
    (.vertex 0.55 0  0.45) 
    (.vertex 0.45 0  0.35) 
          
    (.vertex 0.7 0  0.8) 
    (.vertex 0.8 0  0.7) 
    (.vertex 0.55 0  0.45) 
    (.vertex 0.45 0  0.55) 
        
    (.endShape))))

(defn teleport-shape []
  (let [s (.createShape (q/current-graphics))]
  (doto s
    (.beginShape processing.core.PConstants/QUADS)
    (.fill 0 0 255)
    (.vertex 0     0      0)
    (.vertex 1 0      0)
    (.vertex 1 0      1)
    (.vertex 0     0      1)

    (.vertex 0     1.75 0)
    (.vertex 1 1.75 0)
    (.vertex 1 1.75 1)
    (.vertex 0     1.75 1)

    (.vertex 0     0      0)
    (.vertex 0     1.75 0)
    (.vertex 1 1.75 0)
    (.vertex 1 0      0)

    (.vertex 0     0      0)
    (.vertex 0     1.75 0)
    (.vertex 0     1.75 1)
    (.vertex 0     0      1)

    (.vertex 0     0      1)
    (.vertex 0     1.75 1)
    (.vertex 1 1.75 1)
    (.vertex 1 0      1)

    (.vertex 1 0      1)
    (.vertex 1 1.75 1)
    (.vertex 1 1.75 0)
    (.vertex 1 0      0)
    
    (.vertex 0.2 0  0.2) 
    (.vertex 0.3 0  0.2) 
    (.vertex 0.3 0  0.8) 
    (.vertex 0.2 0  0.8) 
        
    (.vertex 0.7 0  0.2) 
    (.vertex 0.8 0  0.2) 
    (.vertex 0.8 0  0.8) 
    (.vertex 0.7 0  0.8) 
        
    (.endShape))))

(defn finish-shape []
  (box 1 2.5 1 [255 215 0]))

(defn arrow-shape []
  (box 0.1 0.1 0.1 [0 0 0]))

(def start-shape brick-shape)

(defn load [name]
  (q/load-shape (str "shapes/" name ".obj")))

(defn basic
  "Map of all shapes made with a basic box"
  []
  {:cuboid   (cuboid-shape)
   :cube     (cube-shape)
   :brick    (brick-shape)
   :wood     (wood-shape)
   :normal   (normal-shape)
   :strong   (strong-shape)
   :teleport (teleport-shape)
   :start    (start-shape)
   :finish   (finish-shape)
   :arrow    (arrow-shape)})

(defn default
  "Map of all shapes, the ones that should be used in the final version"
  []
  {:cube     (cube-shape)
   :brick    (load "brick")
   :wood     (load "wood")
   :normal   (load "normal")
   :strong   (load "strong")
   :teleport (load "teleport")
   :start    (load "brick")
   :finish   (load "finish")
   :arrow    (load "arrow")})

(defn setup [state]
  (merge state
         {:shapes/shapes (basic)}))
