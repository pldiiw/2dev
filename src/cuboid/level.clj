(ns cuboid.level
  "A level is what the player moves its cubes onto. This namespace contains
  functions to manipulate all state related to a level. It is composed of 4
  keywords:
   * :level/level - A two-dimensional vector representing the arrangement of all
  the blocks the level contains. Each block is a map composed of a :type, and
  either a :to and :toggles. The :type is the kind of a block. It is either a
  :brick, a :wood, :normal or :strong switches, a :teleport, a :finish or a
  :start. Refer to the game manual for an overview of what each type corresponds
  to. The :teleport also contains a :to 2-dimensional vector, its value being a
  :level/pos to teleport the cubes to when they are onto it. :normal and :strong
  have a :toggles vector. Each element in this vector is map with the coordinates
  of a block, :x for the column and :z for the row, and a block map as the value
  of a :to keyword, this block map being what the switch will replace the block
  located at [:x,:z] coords when triggered.
   * :level/pos - A vector containing the coordinates of each cube, in the
  format [column row].
   * :level/time - The time the player has to finish to level, in milliseconds.
   * :level/started-at - The time when the player started the level. Not a date,
  just the number of milliseconds since the sketch started. With :level/time, it
  is used to see if the player still has time to finish the level or not."
  (:require [clojure.edn :as edn]
            [quil.core :as q]
            [clojure.java.io :as io]
            [cuboid.config :as config]
            [cuboid.util :as util]))

(defn start-position
  "Returns the position of the first :start block, as a :level/pos value"
  [level]
  (first
   (for [row   (range (count level))
         col   (range (count (get level row)))
         :when (= (get-in level [row col :type]) :start)]
     [[col row] [col row]])))

(defn names []
  (util/generic-names [(io/resource config/builtin-levels-path) config/user-levels-path]))

(defn save [name {level :level/level time :level/time}]
  (spit (str config/user-levels-path "/" name ".edn") (pr-str {:level level :time time})))

(defn load [name]
  (let [file                 (or (io/resource (str config/builtin-levels-path "/" name ".edn"))
                                 (str config/user-levels-path "/" name ".edn"))
        {:keys [level time]} (edn/read-string (slurp file))]
    {:level/level level
     :level/time  time}))

(defn delete [name]
  (io/delete-file (str config/user-levels-path "/" name ".edn") true)) ; fails silently

(defn indexed
  "Annotate each row and column of a level with its index. Each becomes a vector
  of [index row-or-col]."
  [level]
  (map-indexed vector (map #(map-indexed vector %) level)))

(def memoized-indexed
  "Memoized version of indexed."
  (memoize indexed))

(defn setup [{course :course/course :as state}]
  (let [level-name                      (if course (first course) "empty")
        {level :level/level :as loaded} (-> (load level-name)
                                            (update :level/level #(:level/level state %1))
                                            (update :level/time #(:level/time state %1)))
        pos                             (start-position level)]
    (merge state
           loaded
           {:level/started-at (q/millis)
            :level/pos        pos})))

(defn destroy [state]
  (dissoc state
          :level/level
          :level/pos
          :level/started-at
          :level/time))

(defn reset [state]
  (setup (destroy state)))

(defn draw [level shapes]
  (doseq [[row_index row]       (memoized-indexed level)
          [col_index {t :type}] row]
    (q/push-matrix)
    (q/shape-mode :corner)
    (q/translate col_index 0 row_index)
    (when-let [shape (t shapes)] (q/shape shape))
    (q/pop-matrix)))

(defn time-left
  "Compute the time left the player has to finish the level."
  [started-at time]
  (println started-at time)
  (- time (- (q/millis) started-at)))

(defn shift-links
  "Increment all coords contained in :to and :toggles by x column and z rows for
  each block in the level. This is used to reposition all links after the origin
  the level has changed, i.e. when placing a block at the left or above the
  level in the editor."
  [lvl x z]
  (vec (map
        (fn [row] (vec (map
                        (fn [block]
                          (condp #(contains? %2 %1) block
                            :to      (update block :to (fn [to] (map (fn [[pos-x pos-z]] [(+ pos-x x) (+ pos-z z)]) to)))
                            :toggles (update block :toggles (fn [toggles] (map (fn [{pos-x :x pos-z :z to :to}] {:x (+ pos-x x) :z (+ pos-z z) :to to}) toggles)))
                            block))
                        row)))
        lvl)))

(defn fill-to
  "If the block located at x,z is outside the level, adds rows and columns of
  :empty blocks until x,z isn't nil anymore. Works in all directions (x and z
  can be negative)."
  [lvl x z]
  (if (nil? (get lvl z))
    (if (neg? z)
      (recur (vec (cons [] lvl)) x (inc z))
      (recur (conj lvl []) x z))
    (if (nil? (get-in lvl [z x]))
      (if (neg? x)
        (recur (vec (map #(vec (cons {:type :empty} %1)) lvl)) (inc x) z)
        (recur (update lvl z #(conj %1 {:type :empty})) x z))
      lvl)))

(defn top-down-view-optimal-scale
  "Computes the ratio the shapes should be scaled to in order for the level to
  fit inside the screen. Optimized for a top-down perspective."
  [lvl]
  (let [max-row (apply max (map count lvl))
        max-col (count lvl)
        scale (min (/ (- (q/width) (* (q/width) 0.15)) max-row)
                   (/ (- (q/height) (* (q/height) 0.20)) max-col))]
    (if (neg? scale)
      0
      scale)))

(defn top-down-view-optimal-translate
  "Returns the translation vector indicating the optimal position of the origin
  of the level to fit it the screen entirely. Assumes the drawing cursor is at
  the center of the screen. Optimized for a top-down perspective."
  [lvl]
  (let [max-row (apply max (map count lvl))
        max-col (count lvl)]
    [(* -0.5 max-row)
     (* -0.5 max-col)
     0]))

(defn ingame-view-optimal-translate
  "Same as top-down-view-optimal-translate, but optimized for the perspective
  used ingame."
  [lvl]
  (let [max-row (apply max (map count lvl))
        max-col (count lvl)]
    [(* -0.5 max-row)
     (* -0.5 max-col)
     0]))
