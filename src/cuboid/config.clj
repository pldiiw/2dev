(ns cuboid.config
  "Contains all configuration-related vars. Mainly for defining in a centralized
  fashion all the paths that the game needs."
  (:require [cuboid.util :as util]))

(def ^:const user-prefix "/tmp")

(def ^:const builtin-levels-path "levels")

(def ^:const user-levels-path (str user-prefix "/levels"))

(def ^:const builtin-courses-path "courses")

(def ^:const user-courses-path (str user-prefix "/courses"))

(def ^:const user-runs-path (str user-prefix "/runs"))

(def ^:const user-scores-path (str user-prefix "/scores"))

(def ^:const default-course-name "StoryMode")

(def ^:const window-size (let [w 1920 h (* (/ w 16) 9)] [w h]))  ; 16:9 ratio to width

(defn create-paths
  "Create all configuration path"
  []
  (let [paths [user-prefix
               user-levels-path
               user-courses-path
               user-runs-path
               user-scores-path]]
    (doseq [path paths] (util/mkdirp path))))
