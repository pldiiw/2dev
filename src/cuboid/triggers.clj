(ns cuboid.triggers
  "The triggers namespace collates all behaviours related to the level's blocks.
  This is where all the falling, teleportation, bridging, etc happens."
  (:require [cuboid.level :as level]
            [cuboid.course :as course]
            [cuboid.score :as score]))

(defn action-switch-block
  "Logic for a switch block (:normal and :strong). Simply replace the linked
  blocks by their respective :to. See level namespace if you didn't understand
  this last sentence."
  [state [x z]]
  (let [lvl                    (:level/level state)
        block                  (get-in lvl [z x])
        toggles                (:toggles block)
        ;; Replace the blocks targeted by our toggles by their respective `to`s
        new-lvl                (reduce
                                (fn [old-lvl {to-x :x to-z :z to :to}]
                                  (assoc-in old-lvl [to-z to-x] to))
                                lvl
                                toggles)
        ;; And supersede the old level
        new-state              (assoc state :level/level new-lvl)]
    new-state))

(defn action-fall
  "Logic when falling. This simply reset the level while keeping the
  :level/started-at around."
  [{started-at :level/started-at :as state}]
  (assoc (level/reset state)
         :level/started-at started-at))

(defn weight-block
  "Given the coords of a block and the pos of each cube, compute how many cubes
  are standing on the former."
  [block-pos pos]
  (count (filter (partial = block-pos) pos)))

(defn type-and-weight
  "Used as the dispatch fn in the block-trigger multimethod. This simply
  retrieves the type and weight (with `weight-block`) of the block the given
  cube is standing on."
  [{lvl :level/level pos :level/pos} [x z]]
  (let [type   (get-in lvl [z x :type] :empty)
        weight (weight-block [x z] pos)]
    [type weight]))

(defmulti block-trigger
  "All block's triggers are defined here. That is to say what happens when a
  cube is standing on a normal switch, etc. Each block type has a trigger that
  only gets actioned if its weight is reached (i.e. one or two cubes stand on
  it). This can be specified as the dispatch value, `[type weight]`, with type
  the type of block (e.g. `:brick`) and weight the number of cubes that have to
  be standing on this block."
  type-and-weight)

(defmethod block-trigger [:wood 2] [state cube-pos]
  (action-fall state))

(defmethod block-trigger [:normal 1] [state cube-pos]
  (action-switch-block state cube-pos))
(defmethod block-trigger [:normal 2] [state cube-pos]
  (action-switch-block state cube-pos))

(defmethod block-trigger [:strong 2] [state cube-pos]
  (action-switch-block state cube-pos))

(defn end-of-course-wrapper [{course :course/course name :course/name scored :score/score :as state}]
  (if (nil? course)
    (assoc state
           :scene/next 'cuboid.scenes.text-input/scene
           :text-input/quit-to 'cuboid.scenes.menu/scene
           :text-input/instruction (format "Your final score is %s.\nType in your name to register it." scored)
           :text-input/input-to :text-input/discard-me
           :text-input/transform-input #(score/add-to name %1 scored))
    (level/reset state)))

(defmethod block-trigger [:finish 2] [{[current & nexts] :course/course looping :course/looping :as state} cube-pos]
  (if looping
    (-> state
        (assoc :course/course (conj (vec nexts) current))
        (score/reset)
        (level/reset))
    (-> state
        (assoc :course/course nexts)
        (end-of-course-wrapper))))

(defmethod block-trigger [:teleport 2] [state [x z]]
  (let [new-pos (get-in state [:level/level z x :to])]
    (assoc state :level/pos new-pos)))

(defmethod block-trigger [:empty 1] [state cube-pos]
  (action-fall state))
(defmethod block-trigger [:empty 2] [state cube-pos]
  (action-fall state))

(defmethod block-trigger [:easter-egg 2] [state cube-pos]
  (-> state
      (update :course/course #(vec (cons "Level 11" %1)))
      (level/reset)))

(defmethod block-trigger :default [state cube-pos]
  state)

(defn no-time-left-trigger
  "If the player has no time left, reset the entire course."
  [{time :level/time started-at :level/started-at :as state}]
  (if (neg? (level/time-left started-at time))
    (course/reset state)
    state))

(defn no-time-left-trigger-wrapper
  "Wraps no-time-left-trigger to be used in a threading macro. Takes care of not
  executing the trigger if the timer isn't enabled."
  [{timer-enabled :course/timer-enabled :as state}]
  (if timer-enabled
    (assoc (no-time-left-trigger state)
           :course/timer-enabled timer-enabled)
    state))

(defn action-block-triggers
  "For each cube, execute the trigger it is standing on."
  [{pos :level/pos lvl :level/level :as state}]
  (reduce block-trigger state (distinct pos)))
