(ns cuboid.scene
  (:require [cuboid.scenes.ingame :as scenes.ingame]
            [cuboid.scenes.menu :as scenes.menu]
            [cuboid.scenes.level-editor :as scenes.level-editor]
            [cuboid.scenes.course-editor :as scenes.course-editor]
            [cuboid.scenes.text-input :as scenes.text-input]
            [cuboid.scenes.list-input :as scenes.list-input]
            [cuboid.scenes.high-scores :as scenes.high-scores]))

(defn setup [{{setup :setup} :scene/scene :as state}]
  ((eval setup) state))

(defn update [{{update-state :update destroy :destroy} :scene/scene next :scene/next keep :scene/keep :as state}]
  (if (nil? next)
    ((eval update-state) state)
    (-> state
        ((eval (or destroy 'identity)))
        (merge (select-keys state keep))
        (assoc :scene/scene (eval next))
        (dissoc :scene/next :scene/keep)
        (setup))))

(defn draw [{{draw :draw} :scene/scene :as state}]
  ((eval draw) state))

(defn key-pressed [{{key-pressed :key-pressed} :scene/scene :as state} event]
  (if key-pressed
    ((eval key-pressed) state event)
    state))

(defn mouse-pressed [{{mouse-pressed :mouse-pressed} :scene/scene :as state} event]
  (if mouse-pressed
    ((eval mouse-pressed) state event)
    state))

(defn mouse-wheel [{{mouse-wheel :mouse-wheel} :scene/scene :as state} event]
  (if mouse-wheel
    ((eval mouse-wheel) state event)
    state))

(defn mouse-moved [{{mouse-moved :mouse-moved} :scene/scene :as state} event]
  (if mouse-moved
    ((eval mouse-moved) state event)
    state))
