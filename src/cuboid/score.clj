(ns cuboid.score
  "The score is the number of movements it took the player to finish a course.
  This namespace handles all state related to scoring and provides some
  utilities for the high scores. The score is only one keyword: :score/score,
  which is denoting the number of moves."
  (:require [cuboid.config :as config]
            [cuboid.util :as util]
            [clojure.edn :as edn]))

(defn names []
  (util/generic-names [config/user-scores-path]))

(defn save
  "Score is a special case concerning loading and saving. Save and load don't
  save or load a score, they handle high score tables. These tables are just map
  of player names as keys and their score for a course as value. The table's
  name correspond to the name of the course."
  [name scores]
  (spit (str config/user-scores-path "/" name ".edn") (pr-str scores)))

(defn load
  "Loads a high scores table, see save for more information."
  [name]
  (edn/read-string (slurp (str config/user-scores-path "/" name ".edn"))))

(defn setup [state]
  (assoc state :score/score 0))

(defn destroy [state]
  (dissoc state :score/score))

(defn reset [state]
  (setup (destroy state)))

(defn add-to
  "Add to the high scores table `name` a new entry."
  [name player score]
  (let [scores     (if (contains? (set (names)) name) (load name) {})
        new-scores (update scores player (fnil (partial min score) score))]
    (save name new-scores)))

(defn sort-by-best
  "Given a high scores table, sort it in decreasing order."
  [scores]
  (sort (fn [[_ score1] [_ score2]] (< score1 score2)) scores))
